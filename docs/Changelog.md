#Changelog

- Site support for Crunchyroll.com [2017.03.05]
- Fix for #1 [2017.03.06]
- Fix for #2 [2017.03.06]
- ReadMe updated for Python Script execution [2017.03.06]